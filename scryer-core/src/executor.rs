use std::collections::{HashMap, HashSet};

use scryer_parser::parser::{HttpFile, HttpRequest, VariableDefinition};

use crate::ScryerError;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Executor {
    pub dependencies: HashMap<String, Executor>,
    pub request: HttpRequest,
    pub profile: Option<String>,
}

impl Executor {
    // TODO: this can go into an infinite loop, so should find a way to abort if that happens
    pub fn from_file_by_name(target: &str, file: &HttpFile) -> Self {
        let request = file.get_by_name(target).clone();
        Executor::create(request, file)
    }

    pub fn by_index(idx: usize, file: &HttpFile) -> Self {
        let request = file.get(idx).clone();
        Executor::create(request, file)
    }

    pub fn by_name(name: &str, file: &HttpFile) -> Result<Self, ScryerError> {
        let Some(request) = file
            .requests
            .iter()
            .find(|r| r.name.as_ref().map(|s| s.as_ref()) == Some(name))
        else {
            return Err(ScryerError::RequestNotFound(
                "Could not find request with name: {name}".to_string(),
            ));
        };
        Ok(Executor::create(request.clone(), file))
    }

    pub fn last_from_file(file: &HttpFile) -> Self {
        let len = file.requests.len() - 1;
        let request = file.get(len).clone();
        Executor::create(request, file)
    }

    fn create(request: HttpRequest, file: &HttpFile) -> Self {
        let mut dependencies = HashMap::default();
        for v in &request.variables {
            if let VariableDefinition::Request((ref source_name, _)) = v.source {
                let exec = Executor::from_file_by_name(source_name, file);
                if !dependencies.contains_key(source_name) {
                    dependencies.insert(source_name.to_string(), exec);
                }
            }
        }

        Executor {
            profile: None,
            dependencies,
            request,
        }
    }

    pub fn flatten(self) -> (Vec<HttpRequest>, Option<String>) {
        fn flatten_inner(
            mut dependencies: HashMap<String, Executor>,
            request: HttpRequest,
        ) -> Vec<HttpRequest> {
            let mut to_execute = vec![request];
            for (_, dep) in dependencies.drain() {
                let Executor {
                    dependencies: d,
                    request: r,
                    ..
                } = dep;

                to_execute = [to_execute, flatten_inner(d, r)].concat();
            }

            to_execute
        }

        let Executor {
            dependencies,
            request,
            profile,
        } = self;

        let mut to_execute = flatten_inner(dependencies, request);
        to_execute.reverse();
        to_execute = to_execute
            .into_iter()
            .fold((vec![], HashSet::new()), |(mut acc, mut seen), req| {
                let Some(ref name) = req.name else {
                    acc.push(req);
                    return (acc, seen);
                };
                if seen.contains(name) {
                    return (acc, seen);
                }
                seen.insert(name.clone());
                acc.push(req);
                (acc, seen)
            })
            .0;
        (to_execute, profile)
    }
}

#[cfg(test)]
mod tests {
    use scryer_parser::parser::HttpUrl;

    use super::*;

    #[test]
    fn test_flatten_simple() {
        let a = Executor {
            profile: None,
            dependencies: Default::default(),
            request: HttpRequest::new("a".to_string()),
        };

        let b = Executor {
            profile: None,
            dependencies: HashMap::from_iter([("".to_string(), a)].into_iter()),
            request: HttpRequest::new("b".to_string()),
        };

        let c = Executor {
            profile: None,
            dependencies: HashMap::from_iter([("".to_string(), b)].into_iter()),
            request: HttpRequest::new("c".to_string()),
        };

        let (flatten, _) = c.flatten();
        assert_eq!(3, flatten.len());

        assert_eq!(flatten[0].url, HttpUrl::Raw("a".to_string()));
        assert_eq!(flatten[1].url, HttpUrl::Raw("b".to_string()));
        assert_eq!(flatten[2].url, HttpUrl::Raw("c".to_string()));
    }

    #[test]
    fn test_flatten_branch() {
        let a = Executor {
            profile: None,
            dependencies: HashMap::default(),
            request: HttpRequest::new("a".to_string()),
        };

        let b = Executor {
            profile: None,
            dependencies: HashMap::default(),
            request: HttpRequest::new("b".to_string()),
        };

        let c = Executor {
            profile: None,
            dependencies: HashMap::from_iter(
                [("a".to_string(), a), ("b".to_string(), b)].into_iter(),
            ),
            request: HttpRequest::new("c".to_string()),
        };

        let (flatten, _) = c.flatten();
        assert_eq!(3, flatten.len());

        let urls = [flatten[0].url.clone(), flatten[1].url.clone()];
        assert!(urls.contains(&HttpUrl::Raw("a".to_string())));
        assert!(urls.contains(&HttpUrl::Raw("b".to_string())));
        assert_eq!(flatten[2].url, HttpUrl::Raw("c".to_string()));
    }

    #[test]
    fn test_flatten_two_requiring_same() {
        let mut a = Executor {
            profile: None,
            dependencies: HashMap::default(),
            request: HttpRequest::new("a".to_string()),
        };
        a.request.name = Some("a".to_string());

        let mut b = Executor {
            profile: None,
            dependencies: HashMap::from_iter([("a".to_string(), a.clone())].into_iter()),
            request: HttpRequest::new("b".to_string()),
        };
        b.request.name = Some("b".to_string());

        let mut d = Executor {
            profile: None,
            dependencies: HashMap::from_iter([("a".to_string(), a)].into_iter()),
            request: HttpRequest::new("d".to_string()),
        };
        d.request.name = Some("d".to_string());

        let mut c = Executor {
            profile: None,
            dependencies: HashMap::from_iter(
                [("b".to_string(), b), ("d".to_string(), d)].into_iter(),
            ),
            request: HttpRequest::new("c".to_string()),
        };
        c.request.name = Some("c".to_string());

        let (flatten, _) = c.flatten();
        assert_eq!(4, flatten.len());

        assert_eq!(flatten[0].url, HttpUrl::Raw("a".to_string()));
        let urls = [flatten[1].url.clone(), flatten[2].url.clone()];
        assert!(urls.contains(&HttpUrl::Raw("b".to_string())));
        assert!(urls.contains(&HttpUrl::Raw("d".to_string())));
        assert_eq!(flatten[3].url, HttpUrl::Raw("c".to_string()));
    }
}
