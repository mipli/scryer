mod error;
mod executor;
mod template;

use std::collections::HashMap;

pub use error::ScryerError;
pub use executor::Executor;
use jaq_interpret::{Ctx, Filter, FilterT, ParseCtx, RcIter, Val};
use reqwest::{header::HeaderMap, Url};
use scryer_parser::parser::{HttpRequest, HttpUrl, VariableDefinition};
use serde_json::Value;
use tracing::{debug, info};

pub mod client;

#[derive(Default, Debug)]
pub struct ScryerContext {
    pub variables: HashMap<String, String>,
    pub responses: HashMap<String, String>,
}

pub fn get_iter(executor: Executor, context: ScryerContext) -> ExecIterator {
    let (to_execute, profile) = executor.flatten();

    ExecIterator {
        requests: to_execute,
        context,
        profile,
    }
}

pub struct ExecResult {
    pub content: String,
    pub code: u16,
    pub request: HttpRequest,
}

impl ExecResult {
    pub fn is_success(&self) -> bool {
        self.code >= 200 && self.code < 300
    }
}

pub struct ExecIterator {
    requests: Vec<HttpRequest>,
    context: ScryerContext,
    profile: Option<String>,
}

impl ExecIterator {
    pub fn peek(&self) -> Option<&HttpRequest> {
        if self.requests.is_empty() {
            return None;
        }

        Some(&self.requests[0])
    }

    pub async fn next(&mut self) -> Result<Option<ExecResult>, ScryerError> {
        if self.requests.is_empty() {
            return Ok(None);
        }
        let req = self.requests.remove(0);

        info!("Executing request: {req}");
        let req = prepare_request(&req, &self.context, self.profile.as_ref())?;
        let response = client::execute(&req).await?;
        debug!("Finished: {req}");

        if let Some(ref req_name) = req.name {
            self.context
                .responses
                .insert(req_name.clone(), response.1.clone());
        }

        Ok(Some(ExecResult {
            content: response.1,
            code: response.0,
            request: req,
        }))
    }
}

pub fn prepare_request(
    req: &HttpRequest,
    context: &ScryerContext,
    profile: Option<&String>,
) -> Result<HttpRequest, ScryerError> {
    let variables = extract_variables(req, context, profile)?;
    for (key, value) in &variables {
        info!("Variable: {key} := {value}");
    }
    let url = match req.url {
        HttpUrl::Prepared(ref url) => url.clone(),
        HttpUrl::Raw(ref body) => {
            let url = template::render(body, &variables)?;

            url.parse::<Url>()
                .map_err(|err| ScryerError::InvalidUrl(format!("{err}")))?
        }
        HttpUrl::Empty => todo!(),
    };

    let body = if let Some(ref body) = req.body {
        Some(template::render(body, &variables)?)
    } else {
        None
    };

    let mut headers = HeaderMap::new();
    for (name, value) in req.headers.iter() {
        let val = template::render(
            value
                .to_str()
                .expect("All header values can be converted to strings"),
            &variables,
        )?;
        headers.insert(name, val.parse()?);
    }

    Ok(HttpRequest {
        name: req.name.clone(),
        method: req.method.clone(),
        url: HttpUrl::Prepared(url),
        version: req.version.clone(),
        headers,
        body,
        variables: req.variables.clone(),
    })
}

fn extract_variables(
    req: &HttpRequest,
    context: &ScryerContext,
    profile: Option<&String>,
) -> Result<HashMap<String, String>, ScryerError> {
    let mut variables = HashMap::default();

    for (name, value) in context.variables.iter() {
        variables.insert(name.clone(), value.clone());
    }

    for var in &req.variables {
        match &var.profile {
            // variables with profile require matching profile
            Some(vp) if profile.filter(|&p| p != vp).is_some() => continue,
            // variables without profile does not override previous variables
            None if variables.contains_key(&var.name) => continue,
            _ => {}
        }
        debug!("Fetching variable: {var}");
        let value = match var.source {
            VariableDefinition::Env(_) => context
                .variables
                .get(&var.name)
                .ok_or_else(|| {
                    let name = req.name.clone().unwrap_or("Unnamed request".to_string());
                    ScryerError::MissingVariable((name, var.name.to_string()))
                })?
                .clone(),
            VariableDefinition::Request((ref req_name, ref query)) => {
                if let Some(content) = context.responses.get(req_name) {
                    extract_value(content, query)?
                } else {
                    return Err(ScryerError::RequestNotFound(req_name.clone()));
                }
            }
            VariableDefinition::Text(ref txt) => txt.to_string(),
        };
        variables.insert(var.name.clone(), value);
    }

    Ok(variables)
}

fn prepare_filter(filter: &str) -> Result<Filter, ScryerError> {
    let (f, errs) = jaq_parse::parse(filter, jaq_parse::main());
    if !errs.is_empty() {
        let err_msg = errs
            .iter()
            .map(|e| e.to_string())
            .collect::<Vec<String>>()
            .join("\n");
        return Err(ScryerError::InvalidFilter((filter.to_string(), err_msg)));
    }
    let mut defs = ParseCtx::new(Vec::new());

    if f.is_none() {
        return Ok(Filter::default());
    }

    let f = defs.compile(f.expect("None case already handled"));
    if !defs.errs.is_empty() {
        let err_msg = defs
            .errs
            .iter()
            .map(|(e, _)| e.to_string())
            .collect::<Vec<String>>()
            .join("\n");
        return Err(ScryerError::InvalidFilter((filter.to_string(), err_msg)));
    }

    Ok(f)
}

pub fn extract_value(body: &str, filter: &str) -> Result<String, ScryerError> {
    let input: Value = serde_json::from_str(body)?;

    let f = prepare_filter(filter)?;

    let inputs = RcIter::new(core::iter::empty());

    let mut out = f.run((Ctx::new([], &inputs), Val::from(input)));

    let Some(to) = out.next() else {
        return Ok(String::new());
    };

    let t = to?;
    let val = t
        .as_str()
        .expect("Value parsed from JSON can always be a string")
        .to_string();

    Ok(val)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract_value() {
        let body = "{\"foo\": \"bar\"}";
        let filter = ".foo";
        let value = extract_value(body, filter).unwrap();

        assert_eq!(value, "bar".to_string());
    }
}
