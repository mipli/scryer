use std::collections::{HashMap, HashSet};

use lazy_static::lazy_static;
use regex::Regex;
use tracing::trace;

use crate::ScryerError;

lazy_static! {
    // the variable regex here should match the variable_name rule in http.pest
    static ref VARIABLE_MATCHER: Regex = Regex::new(r"\{\{[a-zA-Z_]{2,50}\}\}").expect("Regex is valid");
}

pub fn render(template: &str, data: &HashMap<String, String>) -> Result<String, ScryerError> {
    trace!("Rendering template: {template}");
    let variables = extract_variables(template);
    trace!("Found variables in template: {:?}", &variables);
    let mut body = template.to_string();
    for var in variables.iter() {
        let val = data.get(&var[2..var.len() - 2]).ok_or_else(|| {
            ScryerError::MissingTemplateVariable((template.to_string(), var.to_string()))
        })?;
        body = body.replace(var, val);
    }
    Ok(body)
}

fn extract_variables(template: &str) -> HashSet<&str> {
    VARIABLE_MATCHER
        .find_iter(template)
        .map(|m| m.as_str())
        .collect::<HashSet<_>>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract_variables() {
        assert_eq!(
            extract_variables("test {{var}} bar"),
            HashSet::from(["{{var}}"])
        );
        assert_eq!(
            extract_variables("test}} {{var}} {bar"),
            HashSet::from(["{{var}}"])
        );
        assert_eq!(
            extract_variables("test {{var}} {{bar}}"),
            HashSet::from(["{{var}}", "{{bar}}"])
        );
        assert_eq!(
            extract_variables("test {{var}} {{{bar}}}"),
            HashSet::from(["{{var}}", "{{bar}}"])
        );
    }
}
