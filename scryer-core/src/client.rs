use std::{str::FromStr, time::Duration};

use reqwest::{header, Client, Method};
use scryer_parser::parser::{HttpRequest, HttpUrl};
use tracing::debug;

use crate::error::ScryerError;

fn create_client() -> Client {
    let mut headers = header::HeaderMap::new();
    // TODO: find a way to update version here automatically
    headers.insert(
        header::USER_AGENT,
        header::HeaderValue::from_static("Scryer/0.1"),
    );
    headers.insert(
        header::CONTENT_TYPE,
        header::HeaderValue::from_static("application/json"),
    );
    headers.insert(
        header::ACCEPT,
        header::HeaderValue::from_static("application/json"),
    );

    Client::builder()
        .timeout(Duration::from_secs(10))
        .default_headers(headers)
        .no_gzip()
        .build()
        .expect("HTTP Client creation cannot fail")
}

pub async fn execute(req: &HttpRequest) -> Result<(u16, String), ScryerError> {
    let HttpUrl::Prepared(url) = &req.url else {
        return Err(ScryerError::InvalidUrl(req.url.to_string()));
    };
    let request = create_client().request(
        Method::from_str(req.method.to_string().as_str()).expect("Method verified by parser"),
        url.clone(),
    );

    let mut builder = request.headers(req.headers.clone());

    if let Some(body) = &req.body {
        if matches!(req.headers.get(header::CONTENT_TYPE), Some(header) if header.to_str().expect("All headers can be converted to strings").contains("form"))
        {
            let form_elements = body
                .lines()
                .filter_map(|line| {
                    line.split_once('=')
                        .map(|(name, value)| [name.trim(), value.trim()])
                })
                .collect::<Vec<_>>();
            debug!("Form elements: {form_elements:?}");
            builder = builder.form(&form_elements);
        } else {
            builder = builder.body(body.clone());
        }
    }

    let res = builder.send().await?;

    let status = res.status();

    let content = res.text().await?;
    Ok((status.as_u16(), content))
}
