use reqwest::header::InvalidHeaderValue;

const MAX_TEMPLATE_ERROR_LEN: usize = 40;

#[derive(Debug, Clone)]
pub enum ScryerError {
    InvalidFilter((String, String)),
    InvalidUrl(String),
    InvalidJson(String),
    RequestNotFound(String),
    MissingVariable((String, String)),
    MissingTemplateVariable((String, String)),
    Reqwest(String),
    InvalidHeaderValue(String),
    JaqFilterError(String),
}

impl std::error::Error for ScryerError {}

impl std::fmt::Display for ScryerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ScryerError::*;
        match self {
            InvalidFilter((flt, err)) => {
                write!(f, "Invalid filter: {flt}\nError: {err}")
            }
            InvalidUrl(url) => {
                write!(f, "Invalid url: {url:?}")
            }
            InvalidJson(msg) => {
                write!(f, "Invalid json: {msg:?}")
            }
            JaqFilterError(msg) => {
                write!(f, "Invalid filter: {msg:?}")
            }
            RequestNotFound(req_name) => {
                write!(f, "Request missing: {req_name:?}")
            }
            MissingVariable((req_name, var)) => {
                write!(f, "Missing variable {var} in request {req_name}")
            }
            MissingTemplateVariable((tmpl, var)) => {
                let tmpl_str = if tmpl.len() > MAX_TEMPLATE_ERROR_LEN {
                    format!("{}...", &tmpl[0..(MAX_TEMPLATE_ERROR_LEN - 3)])
                } else {
                    tmpl.to_string()
                };
                write!(f, "Missing variable {var} required by: {tmpl_str}")
            }
            Reqwest(msg) => {
                write!(f, "HTTP error: {msg}")
            }
            InvalidHeaderValue(msg) => write!(f, "Invalid header value: {msg}"),
        }
    }
}

impl From<reqwest::Error> for ScryerError {
    fn from(err: reqwest::Error) -> Self {
        ScryerError::Reqwest(format!("{err}"))
    }
}

impl From<InvalidHeaderValue> for ScryerError {
    fn from(err: InvalidHeaderValue) -> Self {
        ScryerError::InvalidHeaderValue(format!("{err}"))
    }
}

impl From<serde_json::Error> for ScryerError {
    fn from(err: serde_json::Error) -> Self {
        ScryerError::InvalidJson(format!("{err}"))
    }
}

impl From<jaq_interpret::Error> for ScryerError {
    fn from(err: jaq_interpret::Error) -> Self {
        ScryerError::JaqFilterError(format!("{err}"))
    }
}
