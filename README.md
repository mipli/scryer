# HTTP Requester

Simple CLI tool to read and execute .http files

Scryer tries to follow the [format specification for .http files](https://github.com/JetBrains/http-request-in-editor-spec/blob/master/spec.md) released by JetBrains, but there are lots of features mentioned there that are not supported and Scryer has added it's own features as well.

For a full list of CLI arguments, run `scryer --help`.

## Features

### Simple requests
```
GET eldenring.fanapis.com/api/bosses

### This denotes a delimeter between requests

# They can have comments

GET eldenring.fanapis.com/api/classes
```

### Variables

```
@base = eldenring.fanapis.com
  
### LIST :: fetch all bosses

GET {{base}}/api/bosses
 
### Get specific boss
 
@[first]id = LIST | .data[1].id
@id = LIST | .data[3].id

GET {{base}}/api/bosses/{{id}}
```

`### LIST :: fetch all bosses` - the word between `###` and `::` gives the request a name.

Variables can get their values in three different ways:
 - `@base = eldenring.fanapis.com` - it's a string value
 - `@id = LIST | .data[3].id` - the value is read from the `LIST` request using the jq-query `.data[3].id`.
 - `@id = ENV | eldenring_id` - the value is read from the enviroment variable

And variables can be connected to profiles:
 - `@[first]id = LIST | .data[1].id` - this variable is used when called with the profile `first`

Variables without a profile are used if no matching variable with a profile is found.


### Request bodies and headers

```
@token = ENV | SECRET_TOKEN

POST reqbin.com/echo/post/json
Authorization: bearer {{token}}

{
  "data": "stuff",
}
```

### POST form parsed from key=value pairs

```
POST reqbin.com/echo/post/form
content-type: application/x-www-form-urlencoded

data=stuff
```

Conversion of the `key=value` pair only happens when there is a `content-type` header with a form value.

### Misc

Inspired by: https://github.com/protiumx/rq/
