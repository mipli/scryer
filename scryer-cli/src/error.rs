use std::path::PathBuf;

use scryer_core::ScryerError;
use scryer_parser::ScryerParserError;

pub type CliResult<T> = Result<T, ScryerCliError>;

pub enum ScryerCliError {
    MissingEnvFile(PathBuf),
    InvalidEnvFile(dotenvy::Error),
    MissingEnvValue(Vec<String>),
    Scryer(ScryerError),
    IOError(String),
    ParsingError(ScryerParserError),
}

impl std::error::Error for ScryerCliError {}

impl std::fmt::Display for ScryerCliError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ScryerCliError::*;
        match self {
            MissingEnvFile(path) => {
                write!(f, "Missing .env file: {path:?}")
            }
            InvalidEnvFile(err) => {
                write!(f, "Invalid .env file: {err}")
            }
            IOError(err) => {
                write!(f, "Error while reading IO: {err}")
            }
            MissingEnvValue(vars) => {
                write!(f, "Missing environment variables: {vars:?}")
            }
            Scryer(err) => {
                write!(f, "Failed to execute request: {err}")
            }
            ParsingError(err) => write!(f, "Parsing .http failed: {err}"),
        }
    }
}

impl std::fmt::Debug for ScryerCliError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self}")
    }
}

impl From<dotenvy::Error> for ScryerCliError {
    fn from(err: dotenvy::Error) -> Self {
        ScryerCliError::InvalidEnvFile(err)
    }
}

impl From<ScryerError> for ScryerCliError {
    fn from(err: ScryerError) -> Self {
        ScryerCliError::Scryer(err)
    }
}

impl From<std::io::Error> for ScryerCliError {
    fn from(err: std::io::Error) -> Self {
        ScryerCliError::IOError(format!("{err:?}"))
    }
}

impl From<ScryerParserError> for ScryerCliError {
    fn from(value: ScryerParserError) -> Self {
        ScryerCliError::ParsingError(value)
    }
}
