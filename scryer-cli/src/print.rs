use std::io::Write;

use colored_json::to_colored_json_auto;
use serde_json::Value;
use termcolor::{ColorSpec, StandardStream, WriteColor};

pub fn lines(mut stdout: &mut StandardStream, lines: &[&str], header: Option<&str>, istty: bool) {
    if !istty {
        return;
    }
    if let Some(header) = header {
        stdout.set_color(ColorSpec::new().set_bold(true)).unwrap();
        writeln!(&mut stdout, "{header}").unwrap();
        stdout.set_color(ColorSpec::new().set_bold(false)).unwrap();
    }
    for line in lines {
        println!("{line}");
    }
}

pub fn json(mut stdout: &mut StandardStream, content: &str, istty: bool) {
    if istty {
        stdout.set_color(ColorSpec::new().set_bold(true)).unwrap();
        writeln!(&mut stdout, "Response:").unwrap();
        stdout.set_color(ColorSpec::new().set_bold(false)).unwrap();
        if let Ok(json) = serde_json::from_str::<Value>(content) {
            let pretty = to_colored_json_auto(&json).expect("valid json is pretty-printable");
            println!("{pretty}");
        } else {
            println!("{content}");
        }
    } else {
        println!("{content}");
    }
}
