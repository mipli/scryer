use std::{fs, path::PathBuf, process::exit, str::FromStr};

use clap::{Arg, ArgGroup, ArgMatches, Command};
use error::ScryerCliError;
use inquire::Select;
use scryer_core::{Executor, ScryerContext};
use scryer_parser::parser::{HttpFile, VariableDefinition};
use termcolor::{ColorChoice, StandardStream};
use tracing::{info, trace};
use tracing_subscriber::{util::SubscriberInitExt, EnvFilter};

mod error;
mod print;

use error::CliResult;

#[tokio::main]
async fn main() -> CliResult<()> {
    #[cfg(windows)]
    let _ = colored_json::enable_ansi_support();

    let istty = is_terminal::is_terminal(std::io::stdout());
    let mut stdout = if istty {
        StandardStream::stdout(ColorChoice::Always)
    } else {
        StandardStream::stdout(ColorChoice::Never)
    };

    match execute(&mut stdout, istty).await {
        Ok(()) => return Ok(()),
        Err(ScryerCliError::MissingEnvValue(values)) => {
            print::lines(
                &mut stdout,
                &values
                    .iter()
                    .map(std::string::String::as_str)
                    .collect::<Vec<_>>(),
                Some("Missing env vars"),
                istty,
            );
        }
        Err(ScryerCliError::MissingEnvFile(ref file)) => {
            let path = format!("{file:?}");
            print::lines(&mut stdout, &[&path], Some("Missing env file"), istty);
        }
        Err(ScryerCliError::InvalidEnvFile(ref err)) => {
            print::lines(
                &mut stdout,
                &[&format!("{err}")],
                Some("Invalid env file"),
                istty,
            );
        }
        Err(ScryerCliError::ParsingError(err)) => {
            print::lines(
                &mut stdout,
                &[&format!("{err}")],
                Some("Error parsing .http file"),
                istty,
            );
        }
        Err(ScryerCliError::Scryer(err)) => {
            print::lines(
                &mut stdout,
                &[&format!("{err}")],
                Some("Error executing request"),
                istty,
            );
        }
        Err(ScryerCliError::IOError(ref err)) => {
            print::lines(
                &mut stdout,
                &[&err.to_string()],
                Some("Error reading file"),
                istty,
            );
        }
    };
    exit(1);
}

async fn execute(stdout: &mut StandardStream, istty: bool) -> CliResult<()> {
    let matches = create_cli();

    configure_logging(matches.get_count("verbose"));

    let cli_env_file = matches.get_one::<String>("env").cloned();

    load_env(cli_env_file.as_ref())?;

    let file = matches
        .get_one::<String>("file")
        .expect("filename to request is required");

    let file = scryer_parser::parser::parse_file(file)?;

    if matches.get_flag("parse-only") {
        let lines = file
            .requests
            .iter()
            .map(|r| format!("{r}"))
            .collect::<Vec<_>>();
        print::lines(
            stdout,
            &lines
                .iter()
                .map(std::convert::AsRef::as_ref)
                .collect::<Vec<_>>(),
            Some("Parsed requests"),
            istty,
        );
        return Ok(());
    }

    let mut executor = match (
        matches.get_one::<usize>("request-index"),
        matches.get_one::<String>("request-name"),
    ) {
        (Some(idx), _) => Executor::by_index(*idx, &file),
        (_, Some(name)) => Executor::by_name(name, &file)?,
        _ => {
            let opts = file
                .requests
                .iter()
                .map(|req| format!("{req}"))
                .collect::<Vec<_>>();

            let ans = Select::new("Select request", opts)
                .with_vim_mode(true)
                .raw_prompt()
                .expect("Failed to select valid item");
            Executor::by_index(ans.index, &file)
        }
    };

    executor.profile = matches.get_one::<String>("profile").cloned();
    if let Some(ref profile) = executor.profile {
        info!("Profile selected: {profile}");
    }

    let context = match prepare_context(&executor, &file) {
        Ok(ctx) => ctx,
        Err(ScryerCliError::MissingEnvValue(vars)) => {
            let env_files = find_env_files()?;
            if env_files.is_empty() || cli_env_file.is_some() {
                return Err(ScryerCliError::MissingEnvValue(vars));
            }

            let env_file = Select::new("Select env file", env_files.to_vec())
                .with_vim_mode(true)
                .raw_prompt()
                .expect("Failed to select valid item");

            load_env(Some(&env_file.value))?;

            prepare_context(&executor, &file)?
        }
        Err(err) => return Err(err),
    };

    perform_request(executor, context, stdout, istty).await?;

    Ok(())
}

fn create_cli() -> ArgMatches {
    Command::new("Scryer")
        .version("0.1")
        .about("Execute requests defined in .http files")
        .arg(
            Arg::new("verbose")
                .short('v')
                .action(clap::ArgAction::Count)
                .help("Logging level (v = info, vv=debug, vvv=trace)"),
        )
        .arg(
            Arg::new("parse-only")
                .long("parse-only")
                .required(false)
                .action(clap::ArgAction::SetTrue)
                .help("Parse .http file, but do not execute any reqeusts")
        )
        .arg(
            Arg::new("profile")
                .long("profile")
                .short('p')
                .required(false)
                .help("profile name to use when reading variables")
        )
        .arg(
            Arg::new("request-index")
                .value_parser(clap::value_parser!(usize))
                .long("index")
                .short('i')
                .required(false)
                .help("index of request in .http file to execute. Default is last in file")
        )
        .arg(
            Arg::new("request-name")
                .long("name")
                .short('n')
                .required(false)
                .help("name of request in .http file to execute. Cannot be used with `request-index`.")
        )
        .group(
            ArgGroup::new("request-ident")
                .args(vec!["request-index", "request-name"])
                .required(false),
        )
        .arg(Arg::new("env").long("env").help(".env file to load enviroment variables from"))
        .arg(Arg::new("file").required(true).help("path to .http file"))
        .get_matches()
}

fn load_env<P: Into<PathBuf>>(path: Option<P>) -> CliResult<()> {
    let path: Option<PathBuf> = path.map(|p| p.into());
    let default_env = PathBuf::from_str("./.env").unwrap();
    if let Some(env) = path {
        if env.try_exists().unwrap_or(false) {
            info!("Using environment file: {env:?}");
            dotenvy::from_filename(env)?;
        } else {
            return Err(ScryerCliError::MissingEnvFile(env.clone()));
        }
    } else if default_env.try_exists().unwrap_or(false) {
        info!("Found environment file: ./env");
        dotenvy::dotenv()?;
    }
    Ok(())
}

fn configure_logging(verbosity: u8) {
    let mut filter = EnvFilter::builder()
        .from_env()
        .expect("Failed to build logging base from environment variables");

    #[allow(clippy::wildcard_in_or_patterns)]
    match verbosity {
        0 => {}
        1 => {
            filter = filter
                .add_directive("scryer=info".parse().unwrap())
                .add_directive("scryer_cli=info".parse().unwrap())
                .add_directive("scryer_core=info".parse().unwrap())
                .add_directive("scryer_parser=info".parse().unwrap());
        }
        2 => {
            filter = filter
                .add_directive("scryer=debug".parse().unwrap())
                .add_directive("scryer_cli=debug".parse().unwrap())
                .add_directive("scryer_core=debug".parse().unwrap())
                .add_directive("scryer_parser=debug".parse().unwrap());
        }
        3 | _ => {
            filter = filter
                .add_directive("scryer=trace".parse().unwrap())
                .add_directive("scryer_cli=trace".parse().unwrap())
                .add_directive("scryer_core=trace".parse().unwrap())
                .add_directive("scryer_parser=trace".parse().unwrap());
        }
    }

    tracing_subscriber::fmt()
        .without_time()
        .with_env_filter(filter)
        .finish()
        .init();
}

fn find_env_files() -> CliResult<Vec<String>> {
    let files = fs::read_dir(".")?;
    Ok(files
        .into_iter()
        .filter_map(|file| {
            file.map(|file| {
                let name = format!("{}", file.path().display());
                if name.ends_with(".env") {
                    Some(name)
                } else {
                    None
                }
            })
            .ok()
            .flatten()
        })
        .collect::<Vec<_>>())
}

fn prepare_context(executor: &Executor, file: &HttpFile) -> CliResult<ScryerContext> {
    let mut context = ScryerContext::default();

    let mut missing = vec![];

    for v in file
        .variables
        .iter()
        .chain(executor.request.variables.iter())
        .chain(
            executor
                .dependencies
                .iter()
                .flat_map(|(_, exec)| exec.request.variables.iter()),
        )
    {
        match &v.profile {
            // variable has profile, but none specified for executor, or
            // variables with profile require matching profile
            Some(vp)
                if executor.profile.is_none()
                    || executor.profile.as_ref().filter(|&p| p != vp).is_some() =>
            {
                continue
            }
            // variables without profile does not override previous variables
            None if context.variables.contains_key(&v.name) => continue,
            _ => {}
        }
        let value = match v.source {
            VariableDefinition::Env(ref query) => {
                if let Ok(value) = dotenvy::var(query) {
                    trace!("ENV: {} = {}", v.name, value);
                    Some(value)
                } else {
                    missing.push(query.to_string());
                    None
                }
            }
            VariableDefinition::Request(_) => None,
            VariableDefinition::Text(ref txt) => {
                trace!("Text: {} = {txt}", v.name);
                Some(txt.clone())
            }
        };
        if let Some(value) = value {
            context.variables.insert(v.name.clone(), value);
        }
    }

    if !missing.is_empty() {
        return Err(ScryerCliError::MissingEnvValue(missing));
    }

    Ok(context)
}

async fn perform_request(
    executor: Executor,
    context: ScryerContext,
    stdout: &mut StandardStream,
    istty: bool,
) -> CliResult<()> {
    let mut exec_iter = scryer_core::get_iter(executor, context);

    if let Some(request) = exec_iter.peek() {
        print::lines(
            stdout,
            &[format!("{request}").as_str()],
            Some("Requesting"),
            istty,
        );
    }
    while let Some(res) = exec_iter.next().await? {
        if !res.is_success() {
            print::lines(stdout, &[res.content.as_str()], Some("Failure"), istty);
        }
        if let Some(request) = exec_iter.peek() {
            print::lines(
                stdout,
                &[format!("{request}").as_str()],
                Some("Requesting"),
                istty,
            );
        } else {
            print::json(stdout, &res.content, istty);
            break;
        }
    }
    // let response = scryer_core::exec(executor, &mut context).await?;

    Ok(())
}
