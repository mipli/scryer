#![allow(clippy::empty_docs)]

use std::{fmt::Display, fs, ops::Deref, path::PathBuf, str::FromStr};

use http::{
    header::{HeaderMap, HeaderName},
    Method, Version,
};
use pest::{
    error::Error,
    iterators::{Pair, Pairs},
    Parser,
};
use tracing::{debug, info, trace};
use url::Url;

use crate::ScryerParserError;

#[derive(pest_derive::Parser)]
#[grammar = "http.pest"]
struct HttpParser;

#[derive(Debug, Clone, Eq, PartialEq)]
struct HttpMethod(Method);

impl From<Method> for HttpMethod {
    fn from(value: Method) -> Self {
        HttpMethod(value)
    }
}

impl Default for HttpMethod {
    fn default() -> Self {
        Method::GET.into()
    }
}

impl Deref for HttpMethod {
    type Target = Method;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'i> TryFrom<Pair<'i, Rule>> for HttpMethod {
    type Error = Error<Rule>;

    fn try_from(pair: Pair<'i, Rule>) -> Result<Self, Self::Error> {
        Ok(match pair.as_str() {
            "GET" => Self(Method::GET),
            "POST" => Self(Method::POST),
            "PUT" => Self(Method::PUT),
            "DELETE" => Self(Method::DELETE),
            _ => unreachable!(),
        })
    }
}

impl Display for HttpMethod {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Clone)]
pub enum ParserError {}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct HttpVersion(Version);

impl FromStr for HttpVersion {
    type Err = ParserError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        Ok(HttpVersion(match value {
            "0.9" => Version::HTTP_09,
            "1.0" | "1" => Version::HTTP_10,
            "1.1" => Version::HTTP_11,
            "2.0" | "2" => Version::HTTP_2,
            "3.0" | "3" => Version::HTTP_3,
            _ => unreachable!("Invalid version string: {value}"),
        }))
    }
}

impl Display for HttpVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl From<Version> for HttpVersion {
    fn from(value: Version) -> Self {
        HttpVersion(value)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Variable {
    pub profile: Option<String>,
    pub name: String,
    pub source: VariableDefinition,
}

impl<'i> TryFrom<Pair<'i, Rule>> for Variable {
    type Error = Error<Rule>;

    fn try_from(pair: Pair<'i, Rule>) -> Result<Self, Self::Error> {
        let mut name = None;
        let mut source = None;
        let mut profile = None;
        for item in pair.into_inner() {
            match item.as_rule() {
                Rule::variable_name => {
                    name = Some(item.as_str().to_string());
                }
                Rule::variable_definition => {
                    source = Some(TryInto::<VariableDefinition>::try_into(item)?);
                }
                Rule::profile_name => {
                    profile = Some(item.as_str().to_string());
                }
                _ => {}
            }
        }

        Ok(Variable {
            name: name.unwrap(),
            profile,
            source: source.unwrap(),
        })
    }
}

impl std::fmt::Display for Variable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "@{} = {}", self.name, self.source)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum VariableDefinition {
    Env(String),
    Request((String, String)),
    Text(String),
}

impl<'i> TryFrom<Pair<'i, Rule>> for VariableDefinition {
    type Error = Error<Rule>;

    fn try_from(pair: Pair<'i, Rule>) -> Result<Self, Self::Error> {
        for item in pair.into_inner() {
            match item.as_rule() {
                Rule::variable_env => {
                    let mut name = None;
                    for i in item.into_inner() {
                        if let Rule::text = i.as_rule() {
                            name = Some(i.as_str().to_string());
                        }
                    }
                    return Ok(VariableDefinition::Env(name.unwrap()));
                }
                Rule::variable_request => {
                    let mut name = None;
                    let mut filter = None;
                    for i in item.into_inner() {
                        match i.as_rule() {
                            Rule::request_name => {
                                name = Some(i.as_str().to_string());
                            }
                            Rule::text => {
                                filter = Some(i.as_str().to_string());
                            }
                            _ => {}
                        }
                    }
                    return Ok(VariableDefinition::Request((
                        name.unwrap(),
                        filter.unwrap(),
                    )));
                }
                Rule::text => {
                    return Ok(VariableDefinition::Text(item.as_str().to_string()));
                }
                _ => {}
            }
        }
        unreachable!("Parser should not validate if we can get here")
    }
}

impl std::fmt::Display for VariableDefinition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            VariableDefinition::Env(query) => write!(f, "ENV | {query}"),
            VariableDefinition::Request((name, query)) => write!(f, "{name} | {query}"),
            VariableDefinition::Text(txt) => write!(f, "{txt}"),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum HttpUrl {
    Prepared(Url),
    Raw(String),
    Empty,
}

impl std::fmt::Display for HttpUrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HttpUrl::Prepared(url) => write!(f, "{url}"),
            HttpUrl::Raw(url) => write!(f, "{url}"),
            HttpUrl::Empty => todo!(),
        }
    }
}

impl From<String> for HttpUrl {
    fn from(value: String) -> Self {
        if value.contains('{') {
            HttpUrl::Raw(value)
        } else {
            HttpUrl::Prepared(value.parse::<Url>().unwrap())
        }
    }
}

#[derive(Clone, Eq, PartialEq)]
pub struct HttpRequest {
    pub name: Option<String>,
    pub method: Method,
    pub url: HttpUrl,
    pub version: HttpVersion,
    pub headers: HeaderMap,
    pub body: Option<String>,
    pub variables: Vec<Variable>,
}

impl<'i> TryFrom<Pair<'i, Rule>> for HttpRequest {
    type Error = Error<Rule>;

    fn try_from(pair: Pair<'i, Rule>) -> Result<Self, Self::Error> {
        debug!("Parsing HttpRequest");
        let mut ret = Self {
            name: None,
            method: Method::GET,
            url: HttpUrl::Empty,
            version: Version::HTTP_11.into(),
            headers: HeaderMap::new(),
            body: None,
            variables: Default::default(),
        };

        for item in pair.into_inner() {
            match item.as_rule() {
                Rule::method => {
                    ret.method = TryInto::<HttpMethod>::try_into(item)?.0;
                }
                Rule::uri => {
                    let uri = item.as_str();
                    let uri = if uri.starts_with("http") {
                        uri.to_owned()
                    } else {
                        format!("https://{uri}")
                    };
                    trace!("Found url: {}", uri);
                    ret.url = HttpUrl::from(uri);
                }
                Rule::headers => {
                    ret.parse_headers(item.into_inner());
                    trace!("Found headers: {:?}", ret.headers);
                }
                Rule::body => {
                    let b = item.as_str().trim();
                    if !b.is_empty() {
                        ret.body = Some(b.to_string());
                        trace!(
                            "Found body, size: {}",
                            ret.body.as_ref().map(|s| s.len()).unwrap()
                        );
                    }
                }
                Rule::version => {
                    ret.version = item.as_str().parse().unwrap();
                    trace!("Found version: {}", ret.version);
                }
                Rule::variable => {
                    let variable = TryInto::<Variable>::try_into(item)?;
                    ret.variables.push(variable);
                }
                Rule::text => {}
                _ => {
                    // pest grammer ensures we cannot get to this point
                    unreachable!();
                }
            }
        }
        trace!("HttpRequest: {ret:?}");

        Ok(ret)
    }
}

impl HttpRequest {
    pub fn new(url: String) -> Self {
        HttpRequest {
            name: None,
            method: Method::GET,
            url: HttpUrl::Raw(url),
            version: "1.0".parse().unwrap(),
            headers: HeaderMap::default(),
            body: None,
            variables: vec![],
        }
    }

    fn parse_headers(&mut self, pairs: Pairs<Rule>) {
        for item in pairs {
            let mut kv = item.into_inner();
            let key: HeaderName =
                HeaderName::from_lowercase(kv.next().unwrap().as_str().to_lowercase().as_bytes())
                    .unwrap();
            let value = kv.next().unwrap().as_str().to_string();

            self.headers.insert(key, value.parse().unwrap());
        }
    }
}

impl Display for HttpRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(ref name) = self.name {
            write!(f, "[{name}] ")?;
        }
        write!(f, "{} {} {}", self.method, self.url, self.version)?;
        let headers = self
            .headers
            .iter()
            .map(|(k, v)| {
                if v.is_sensitive() {
                    format!("{k}: ???")
                } else {
                    format!("{k}: {v:?}")
                }
            })
            .collect::<Vec<_>>()
            .join(", ");
        if !headers.is_empty() {
            write!(f, " [{headers}]")?;
        }
        if self.body.is_some() {
            write!(f, " <BODY>")?;
        }
        Ok(())
    }
}

impl std::fmt::Debug for HttpRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.method, self.url, self.version)?;
        let headers = self
            .headers
            .iter()
            .map(|(k, v)| {
                if v.is_sensitive() {
                    format!("{}: ???", k)
                } else {
                    format!("{}: {:?}", k, v)
                }
            })
            .collect::<Vec<_>>()
            .join(", ");
        if !headers.is_empty() {
            write!(f, " [{headers}]")?;
        }
        let variables = self
            .variables
            .iter()
            .map(|v| format!("{v}"))
            .collect::<Vec<_>>()
            .join(", ");
        if !variables.is_empty() {
            write!(f, " ({variables})")?;
        }
        if self.body.is_some() {
            write!(f, " <BODY>")?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct HttpFile {
    pub variables: Vec<Variable>,
    pub requests: Vec<HttpRequest>,
}

impl<'i> TryFrom<Pair<'i, Rule>> for HttpFile {
    type Error = Error<Rule>;

    fn try_from(pair: Pair<Rule>) -> Result<Self, Self::Error> {
        let iterator = pair.into_inner();
        let mut requests = vec![];
        let mut name = None;
        let mut variables = vec![];
        for item in iterator {
            match item.as_rule() {
                Rule::EOI => {
                    break;
                }
                Rule::delimiter => {
                    for d_item in item.into_inner() {
                        if let Rule::request_name = d_item.as_rule() {
                            name = Some(d_item.as_str().to_string());
                        }
                    }
                }
                Rule::request => {
                    let mut request: HttpRequest = item.try_into()?;
                    request.name = name;
                    requests.push(request);
                    name = None;
                }
                Rule::variable => {
                    let variable = TryInto::<Variable>::try_into(item)?;
                    variables.push(variable);
                }
                _ => {}
            }
        }
        Ok(Self {
            variables,
            requests,
        })
    }
}

impl HttpFile {
    pub fn get(&self, idx: usize) -> &HttpRequest {
        &self.requests[idx]
    }

    pub fn get_by_name(&self, target: &str) -> &HttpRequest {
        self.requests
            .iter()
            .find(|r| r.name.as_deref() == Some(target))
            .unwrap()
    }
}

impl Display for HttpFile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.requests.is_empty() {
            write!(f, "No requests found")?;
            return Ok(());
        }
        let files = self
            .requests
            .iter()
            .enumerate()
            .map(|(i, req)| format!("#{i}\n{req}"))
            .collect::<Vec<_>>()
            .join("\n");
        write!(f, "{files}")
    }
}

#[allow(clippy::result_large_err)]
pub fn parse(input: &str) -> Result<HttpFile, ScryerParserError> {
    info!("Parsing .http input");
    let file = HttpParser::parse(Rule::file, input.trim_start())?
        .next()
        .unwrap();
    Ok(HttpFile::try_from(file)?)
}

#[allow(clippy::result_large_err)]
pub fn parse_file<P: Into<PathBuf>>(file: P) -> Result<HttpFile, ScryerParserError> {
    let file: PathBuf = file.into();
    if let Some(path_str) = file.to_str() {
        info!("Parsing file: {}", path_str);
    }
    let content = fs::read_to_string(file).unwrap();
    let file = HttpParser::parse(Rule::file, content.trim_start())?
        .next()
        .unwrap();
    let http_file = HttpFile::try_from(file)?;
    info!("Found requests: {}", http_file.requests.len());
    Ok(http_file)
}

#[cfg(test)]
mod tests {
    use http::Method;

    use super::*;

    fn assert_parse(input: &str) -> HttpFile {
        let parsed = parse(input);
        parsed.unwrap()
    }

    #[test]
    fn test_empty_input() {
        let file = assert_parse("");
        assert_eq!(file.to_string(), "No requests found");
    }

    #[test]
    fn test_http_methods() {
        [Method::GET, Method::POST, Method::PUT, Method::DELETE]
            .iter()
            .into_iter()
            .for_each(|method| {
                let input = format!("{} https://example.com HTTP/1.1\n\n", method);
                let file = assert_parse(input.as_str());
                assert_eq!(file.requests.len(), 1);
                assert_eq!(
                    file.requests[0].to_string(),
                    format!("{} https://example.com/ HTTP/1.1", method)
                );
            });
    }

    #[test]
    fn test_http_headers() {
        let input = r#"
POST https://example.com HTTP/1.0
authorization: Bearer xxxx

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(
            file.requests[0].headers.get("authorization").unwrap(),
            "Bearer xxxx"
        );
    }

    #[test]
    fn test_http_body_display() {
        let input = r#"
POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(
            file.requests[0].to_string(),
            "POST https://example.com/ HTTP/1.0 [authorization: \"Bearer xxxx\"] <BODY>"
        );
    }

    #[test]
    fn test_http_body() {
        let input = r#"
POST https://example.com HTTP/1.0

{ "test": "body" }"#;
        let file = assert_parse(input);
        assert_eq!(
            file.requests[0].body,
            Some("{ \"test\": \"body\" }".to_string())
        );
    }

    #[test]
    fn test_http_file() {
        let input = r#"
POST https://example.com
authorization: token

###

GET example.com HTTP/1.1

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests.len(), 2);
        assert_eq!(
            file.to_string(),
            "#0\nPOST https://example.com/ HTTP/1.1 [authorization: \"token\"]\n#1\nGET https://example.com/ HTTP/1.1"
        );
    }

    #[test]
    fn test_comments() {
        let input = r#"
# Comment 1

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

// Comment 2

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(
            file.requests[0].to_string(),
            "POST https://example.com/ HTTP/1.0 [authorization: \"Bearer xxxx\"] <BODY>"
        );
    }

    #[test]
    fn test_extract_text_variables() {
        let input = r#"
@foo = test variable

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(file.requests[0].variables.len(), 1);
        assert_eq!(
            file.requests[0].variables[0],
            Variable {
                name: "foo".to_string(),
                profile: None,
                source: VariableDefinition::Text("test variable".to_string()),
            }
        );
    }

    #[test]
    fn test_extract_variable() {
        let input = r#"
@foo = req | .value

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(file.requests[0].variables.len(), 1);
    }

    #[test]
    fn test_extract_env_variables() {
        let input = r#"
@foo = ENV | name

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(file.requests[0].variables.len(), 1);
    }

    #[test]
    fn test_extract_multiple_variables() {
        let input = r#"
@foo = ENV | name
@bar = ENV | name
@bar = foo | .id

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].headers.len(), 1);
        assert_eq!(file.requests[0].variables.len(), 3);
    }

    #[test_log::test]
    fn test_name() {
        let input = r#"
### FOO :: request with name

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.requests[0].name, Some("FOO".to_string()));
    }

    #[test_log::test]
    fn test_global_variables() {
        let input = r#"
@var = adsf

###

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.variables.len(), 1);
    }

    #[test_log::test]
    fn test_profile_variables() {
        let input = r#"
@[foo]var = asdf

###

POST https://example.com HTTP/1.0
authorization: Bearer xxxx

        { "test": "info" }

"#;
        let file = assert_parse(input);
        assert_eq!(file.variables.len(), 1);
        assert_eq!(
            file.variables[0],
            Variable {
                name: "var".to_string(),
                profile: Some("foo".to_string()),
                source: VariableDefinition::Text("asdf".to_string()),
            }
        );
    }
}
