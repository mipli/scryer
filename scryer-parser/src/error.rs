use crate::parser::Rule;

#[derive(Debug, Clone)]
pub enum ScryerParserError {
    ParsingError(String),
}

impl std::error::Error for ScryerParserError {}

impl std::fmt::Display for ScryerParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ScryerParserError::*;
        match self {
            ParsingError(msg) => write!(f, "Parsing error: {msg}"),
        }
    }
}

impl From<pest::error::Error<Rule>> for ScryerParserError {
    fn from(err: pest::error::Error<Rule>) -> Self {
        ScryerParserError::ParsingError(format!("{err}"))
    }
}
